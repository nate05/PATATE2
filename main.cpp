// "Copyright 2018 Nathan Poirier"
#include "Game.h"
#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"

using namespace std;

/*  POUR COMPILER SUR WINDOWS
  g++ -o PATATE main.cpp Game.cpp World.cpp Player.cpp Laser.cpp assets/my.res -IC:\Users\Nathan\SFML\include -LC:\Users\Nathan\SFML\lib -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio

  POUR COMPILER SUR LINUX
 g++ -o PATATE main.cpp Game.cpp World.cpp Player.cpp Laser.cpp -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio
*/

int main() {
  Game game;
  game.run();
}
