// "Copyright 2018 Nathan Poirier"
#include "World.h"
#include <iostream>

using namespace std;

World::World() {
  //  Le 1000 - HEIGHT est pour scaler le background
  backgroundTexture.loadFromFile("assets/background_long.png",
  sf::IntRect(0, 1000 - HEIGHT, 12222, HEIGHT));
  backgroundSprite.setTexture(backgroundTexture);
  houseTexture.loadFromFile("assets/house.png",
  sf::IntRect(0, 0, 247, 385));
  houseSprite.setTexture(houseTexture);
  music.openFromFile("assets/music02.wav");
  music.play();
}

World::~World() {
}

float World::getposx() {
  return posx;
}

void World::setposx(float m_posx) {
  posx = m_posx;
}

void World::update(float dt) {
  backgroundSprite.setPosition(posx, 0);
  houseSprite.setPosition(posx + 325, HEIGHT - 320);
}

void World::draw(sf::RenderWindow &window) {
  window.draw(backgroundSprite);
  window.draw(houseSprite);
}
