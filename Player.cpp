// "Copyright 2018 Nathan Poirier"
#include "Player.h"
#include <iostream>
#include <string>

using namespace std;

Player::Player() : velx(0), vely(0), maxVel(5.0f), posx(WIDTH/2),
posy(HEIGHT), gravity(0.9f), state("default"),
friction(0.1f), jumpHeight(PLAYERH * 3/4), groundPos(HEIGHT) {
  playerTexture.loadFromFile("assets/cat.png");
  playerTexture.setSmooth(true);
  playerSprite.setTexture(playerTexture);
  playerSprite.setScale(1.5f, 1.5f);
  playerSprite.setOrigin(0, PLAYERH / 2);
}


Player::~Player() {
}

float Player::getposx() {
  return posx;
}
float Player::getposy() {
  return posy;
}

float Player::getvelx() {
  return velx;
}
float Player::getvely() {
  return vely;
}
int Player::getLaserState() {
  return laserState;
}
string Player::getFacing() {
  return facing;
}

void Player::setposx(float m_posx) {
  posx = m_posx;
}
void Player::setposy(float m_posy) {
  posy = m_posy;
}

void Player::setvelx(float m_velx) {
  velx = m_velx;
}
void Player::setvely(float m_vely) {
  vely = m_vely;
}
void Player::setState(std::string m_state) {
  state = m_state;
}

void Player::boundaries() {
  //  keeps the player inside the window
  if (posy >= HEIGHT) {
    posy = HEIGHT;
    vely = 0;
  }
  //  player's speed cannot exceeds maxVel
  if (velx > maxVel) {
    velx = maxVel;
  }
  if (velx < -maxVel) {
    velx = -maxVel;
  }
  if (vely > maxVel) {
    vely = maxVel;
  }
  if (vely < -maxVel) {
    vely = -maxVel;
  }
  //  gravity and friction
  if (state != "ground") {
    vely -= gravity;
  }
  if ((state == "ground") && (velx < -0.1)) {
    velx += friction;
  }
  if ((state == "ground") && (velx > 0.1)) {
    velx -= friction;
  }
}

void Player::keyHandle() {
  //  Key handling for the main player
  if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) ||
  (sf::Keyboard::isKeyPressed(sf::Keyboard::A))) {
    velx -= 0.4f;
  }

  if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) ||
  (sf::Keyboard::isKeyPressed(sf::Keyboard::D))) {
    velx += 0.4f;
  }

  if (((sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) ||
  (sf::Keyboard::isKeyPressed(sf::Keyboard::W))) &&
  (state == "ground")) {
    vely += 7.0f;
  }

  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
    if (facing == "right") {
      playerTexture.loadFromFile("assets/cat_laser.png");
    }
    if (facing == "left") {
      playerTexture.loadFromFile("assets/cat_back_laser.png");
    }
  laserState = 1;
  }
}

void Player::stateHandle() {
  //  states of the cat
  if (posy < (groundPos - jumpHeight)) {
    state = "up";
  }
  if (posy > (groundPos - 1)) {
    state = "ground";
  }
}

void Player::movement() {
  //  movement of the cat
  posx += velx;
  posy -= vely;
  if ((posx < WIDTH/3) && (velx < 0)) {
    posx = WIDTH/3;
  }
  if ((posx > WIDTH*2/3) && (velx > 0)) {
    posx = WIDTH * 2/3;
  }
  playerSprite.setPosition(posx, posy);
}

void Player::textureHandle() {
  // sets the texture so the cat faces the right direction
  if (facing == "right") {
    playerTexture.loadFromFile("assets/cat.png");
  }
  if (facing == "left") {
    playerTexture.loadFromFile("assets/cat_back.png");
  }
}

void Player::facingHandle() {
  if (velx > 0) {
    facing = "right";
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
      facing = "left";
    }
  }
  if (velx < 0) {
    facing = "left";
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
      facing = "right";
    }
  }
}

void Player::update(float dt) {
  laserState = 0;
  movement();
  boundaries();
  textureHandle();
  keyHandle();
  stateHandle();
  facingHandle();
}

void Player::draw(sf::RenderWindow &window) {
  window.draw(playerSprite);
}
