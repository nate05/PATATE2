// "Copyright 2018 Nathan Poirier"
#include "Game.h"
#include <iostream>
#include <ctime>

using namespace std;

Game::Game() : window(sf::VideoMode(WIDTH, HEIGHT), "PATATE"), i(0) {
  window.setFramerateLimit(60);
  laserT_B = std::clock();
  laserT_E = std::clock();
}

void Game::run() {
  while (window.isOpen()) {
      processEvents();
      update();
      render();
  }
}

void Game::processEvents() {
  sf::Event event;
  while (window.pollEvent(event)) {
    switch (event.type) {
    case sf::Event::Closed:
      window.close();
      break;
    }
  }
}

void Game::update() {
  player.update(time.asSeconds());
  world.update(time.asSeconds());
  camera();
  laserHandle();
  for (int j = 0; j < LASER; j++) {
    laser[j].update(time.asSeconds());
  }
  clock.restart().asSeconds();
}

void Game::render() {
  window.clear();
  world.draw(window);
  // mettre tout les draw ici
  player.draw(window);
  for (int u = 0; u < LASER; u++) {
    laser[u].draw(window);
  }
  window.display();
}

void Game::camera() {
  if ((player.getposx() < WIDTH/3 + 1) && (world.getposx() < 0)) {
    float newposx = world.getposx() - player.getvelx();
    world.setposx(newposx);
  }
  if (player.getposx() > WIDTH*2/3 - 1) {
    float newposx = world.getposx() - player.getvelx();
    world.setposx(newposx);
  }
}

void Game::laserHandle() {
  if (player.getLaserState() == 1) {
    clock_t timediff = laserT_E - laserT_B;
    float milli = CLOCKS_PER_SEC / 1000;
    clock_t timediff_milli = timediff / milli;
    if (timediff_milli < 50) {
      laserT_E = std::clock();
    } else {
      laserT_B = std::clock();
      laserT_E = std::clock();
      if (player.getFacing() == "right") {
        laser[i].setpos((player.getposx()), (player.getposy()) - 35);
        laser[i].setvelx(-15.0f);
      }
      if (player.getFacing() == "left") {
        laser[i].setpos((player.getposx()) + 70,
        (player.getposy()) - 35);
        laser[i].setvelx(15.0f);
      }
      i += 1;
      if (i == LASER) {
        i = 0;
      }
    }
  }
}
