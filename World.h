// "Copyright 2018 Nathan Poirier"
#include "Player.h"
#include <SFML/Graphics.hpp>
#include "SFML/Audio.hpp"

#ifndef WORLD_H_
#define WORLD_H_

class World {
 public:
  World();
  ~World();
  float getposx();
  void setposx(float m_posx);
  void update(float dt);
  void draw(sf::RenderWindow &window);

 private:
  float posx;
  float posy;
  sf::Texture backgroundTexture;
  sf::Sprite backgroundSprite;
  sf::Texture houseTexture;
  sf::Sprite houseSprite;
  sf::Music music;
};


#endif  // WORLD_H_
